public class EncriptadorFinal {
	//Atributos
	private String mensajeInicial = "";
	private String mensajeFinal = "";
	//Constructor
	public EncriptadorFinal() {
		this.mensajeInicial = new String("");
		this.mensajeFinal = new String("");
	}
	//Getter para el mensaje inicial
	public String getMensaje() {
		return mensajeInicial;
	}
	//Getter para el mensaje final
	public String getMensajeFinal(String mensajeFinal) {
		return mensajeFinal;
	}
	//Setter para mensaje Inicial
	public void setMensajeInicial(String mensajeInicial) {
		this.mensajeInicial = mensajeInicial;
	}
	//Setter para mensaje Final
	public void setMensajeFinal() {
		this.mensajeFinal = mensajeFinal;
	}
	public String encriptador() {
		int length = mensajeInicial.length();
		
		//Cambiamos la primera palabra de la cadena
	    for (int i=0;i<mensajeInicial.length();i++) {
	    	mensajeInicial = mensajeInicial.replaceFirst("PACO","WILLY");
	    }
		//Reemplazamos caracteres
		for (int i=0;i<mensajeInicial.length();i++){
			mensajeFinal = mensajeInicial.replace('E','S').replace('A','E').replace('T','R').replace('E','G').replace('O','I').replace('L','O');
		}
		
		//Reemplazamos los espacios
		mensajeFinal = mensajeFinal.replaceAll("\\s+","-ESPACIO-");
		//Devolvemos el valor
		return mensajeFinal;
	}

}
